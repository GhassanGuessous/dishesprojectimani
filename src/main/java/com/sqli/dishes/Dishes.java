package com.sqli.dishes;

import java.util.*;
import java.util.stream.Stream;

import static com.sqli.dishes.CanalParser.parseCanalSatellite;
import static com.sqli.dishes.SignalCalculator.calculateSignalQuality;

public class Dishes {
	private Map<Integer, Dish> dishes = new HashMap<>();
	private Presenter presenter;
	
	private void init(){
		presenter = new DefaultPresenter();
	}
	
	public Dishes(String... dishesArgs) {
		init();
		for (String dishArgs : dishesArgs) {
			int dishId = DishParser.parseId(dishArgs);
			double orientationValue = DishParser.parseOrientationValue(dishArgs);
			String orientationPoint = DishParser.parseOrientationPoint(dishArgs);
			dishes.put(dishId, new Dish(orientationValue, orientationPoint));
		}
	}

	public String signal(String canal) {
//		String canalName = parseCanalName(canal);
		String satelliteAbbreviation = parseCanalSatellite(canal);
		Satellite satellite = Satellite.from(satelliteAbbreviation);
		//int signalQuality = calculateSignalQuality(satellite, maxDishValueForSatellite(satellite));
		int signalQuality = calculateSignalQuality(satellite, getBestOrientation(satellite));
		return presenter.present(signalQuality);
	}

    public void move(String dishId, String newOrientation) {
		int id = Integer.parseInt(dishId);
		Dish concernedDish = dishes.get(id);
		double orientationValue = OrientationParser.parseOrientationValue(newOrientation);
		String orientationPoint = OrientationParser.parseOrientationPoint(newOrientation);
		concernedDish.move(orientationValue, orientationPoint);
	}
	
	
	/*private double maxDishValueForSatellite(Satellite satellite){
		List<Double> dishesValues = new ArrayList<>();
		for (Dish dish : dishes.values()) {
			if(dish.hasAsOrientationPoint(satellite.getOrientationPoint())){
				dishesValues.add(dish.getOrientationValue());
			}
		}
		Collections.sort(dishesValues);
		int lastIndex = dishesValues.size() - 1;
		return dishesValues.get(lastIndex);
	}*/

    private double getBestOrientation(Satellite satellite) {
       return  dishes.
                values()
                .stream()
		   		.filter(dish -> dish.hasAsOrientationPoint(satellite.getOrientationPoint()))
                .map(Dish::getOrientationValue)
                .min(Comparator.comparingDouble(orientation -> Math.abs(orientation - satellite.getOrientationValue())))
                .get();
    }

}
