package com.sqli.dishes;

public class DishParser {
	private static final String SEPARATOR = ",";

	public static int parseId(String dishArgs) {
		int separatorIndex = dishArgs.indexOf(SEPARATOR);
		return Integer.parseInt(dishArgs.substring(0, separatorIndex));
	}

	public static double parseOrientationValue(String dishArgs) {
		String params[] = dishArgs.split(SEPARATOR);
		String orientationData = params[1];
		return OrientationParser.parseOrientationValue(orientationData);
	}

	public static String parseOrientationPoint(String dishArgs) {
		String params[] = dishArgs.split(SEPARATOR);
		String orientationData = params[1];
		return OrientationParser.parseOrientationPoint(orientationData);
	}
}
