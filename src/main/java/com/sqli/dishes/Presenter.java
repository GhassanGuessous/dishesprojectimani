package com.sqli.dishes;

public interface Presenter {

	String present(int signalQuality);

}
