package com.sqli.dishes;

public class SignalCalculator {

    public static final int QUALITY = 10;

    public static int calculateSignalQuality(Satellite satellite, double maxDishValueForSatellite) {
        double satelliteOrientationValue = satellite.getOrientationValue();
        int deviation = getDeviation(maxDishValueForSatellite, satelliteOrientationValue);
        return QUALITY - deviation;
    }

    private static int getDeviation(double maxDishValueForSatellite, double satelliteOrientationValue) {
        double deviation = Math.abs(satelliteOrientationValue - maxDishValueForSatellite);
        return (int) Math.round(deviation * 10.0);
    }

}
