package com.sqli.dishes;

public class OrientationParser {
	
	public static double parseOrientationValue(String orientationArgs) {
		int pointIndex = orientationArgs.length() - 1;
		double orientationValue = Double.parseDouble(orientationArgs.substring(0, pointIndex));
		return orientationValue;
	}

	public static String parseOrientationPoint(String orientationArgs) {
		int pointIndex = orientationArgs.length() - 1;
		return orientationArgs.substring(pointIndex);
	}
}
