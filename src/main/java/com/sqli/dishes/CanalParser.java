package com.sqli.dishes;

public class CanalParser {
	
	private static final String SEPARATOR = ",";
	
	public static String parseCanalName(String canal) {
		int separatorIndex = canal.indexOf(SEPARATOR);
		return canal.substring(0, separatorIndex);
	}
	
	
	public static String parseCanalSatellite(String canal) {
		int separatorIndex = canal.indexOf(SEPARATOR);
		String satelliteAbbreviation = canal.substring(separatorIndex + 1).trim();
		return satelliteAbbreviation;
	}
}
