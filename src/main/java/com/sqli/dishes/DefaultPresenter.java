package com.sqli.dishes;

public class DefaultPresenter implements Presenter {

    @Override
    public String present(int signalQuality) {
        if (signalQuality <= 0) {
            return "No signal !";
        }
        String result = "|";
        for (int i = 0; i < signalQuality; i++) {
            result += "*";
        }
        for (int i = signalQuality; i < 10; i++) {
            result += ".";
        }
        return result + "|";
    }

}
