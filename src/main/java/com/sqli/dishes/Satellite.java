package com.sqli.dishes;

public enum Satellite {
	
	
	ASTRA("A",19.2,"E"),
	HOTBIRD("H",13.0,"E"),
	NILESAT("N",7.0,"W");
	
	private String abbreviation ;
	private double orientationValue;
	private String orientationPoint;
	
	Satellite(String abbreviation, double orientationValue, String orientationPoint) {
		this.abbreviation  = abbreviation;
		this.orientationValue = orientationValue;
		this.orientationPoint = orientationPoint;
	}

	public String getOrientationPoint() {
		return orientationPoint;
	}
	
	public double getOrientationValue() {
		return orientationValue;
	}


	private boolean hasAbbreviation(String abbreviation) {
		return this.abbreviation.equals(abbreviation);
	}

	public static Satellite from(String abbreviation) {
		for (Satellite satellite : Satellite.values()) {
			if (satellite.hasAbbreviation(abbreviation)) {
				return satellite;
			}
		}
		throw new IllegalArgumentException();
	}
	
	
}
