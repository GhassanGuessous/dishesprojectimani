package com.sqli.dishes;

public class Dish {
	private double orientationValue;
	private String orientationPoint;

	public Dish(double orientationValue, String orientationPoint) {
		this.orientationValue = orientationValue;
		this.orientationPoint = orientationPoint;
	}

	public boolean hasAsOrientationPoint(String orientationPoint) {
		return this.orientationPoint.equals(orientationPoint);
	}

	public void move(double orientationValue, String orientationPoint) {
		if(hasAsOrientationPoint(orientationPoint)){
			this.orientationValue += orientationValue;
		}else{
			moveToAnotherOrientation(orientationValue, orientationPoint);
		}
	}

	private void moveToAnotherOrientation(double orientationValue, String orientationPoint) {
		if(this.orientationValue - orientationValue >= 0){
			this.orientationValue -= orientationValue;
		}else{
			this.orientationValue = Math.abs(this.orientationValue -= orientationValue);
			this.orientationPoint = orientationPoint;
		}
	}

	public double getOrientationValue() {
		return orientationValue;
	}
}
